---
repository: https://github.com/jfcmacro/commandlambdastream.git
tag: lambda-v2.0
origin: git@gitlab.com:jfcmacro/improving-lambda-expression-story-java.git
---

# Improving Lambda Expression Story

**Estimated reading time**: 20 minutes

## Story Outline
Programming paradigms represent different models of computers and different models of thought for solving problems.  They teach us how to program a computer and which strategies we can use to implement our programs. But the paradigms apply to particular programming languages and can be mixed to obtain the better of those words. Since several years ago, the major programming languages, like C#, Java, and C++, have included some essential characteristics from other programming paradigms, as currently happed with the inclusion of anonymous functions, higher-order functions, etc.

This integration between two paradigms (object-oriented programming and functional programming) allows the emergence of new mechanisms, such as new syntactic constructors or syntactic sugar, making writing more concise code easier.

In this story, we will tell you how to improve your code that already used lambda expressions with some new constructors and syntactic sugar that enables you to code more concisely and shorter.

## Story Organization

**Story Branch**: main

> `git checkout main`

**Practical task tag for self-study**: task

> `git checkout task`

Tags: #lambda_expresions, #reference_methods, #streams

---
focus:
---

### Improving Lambda Expression Story

This story will tell you how to improve your code that already uses lambda expressions.

Our story will start with a table-based database implemented with lambda expressions. We will show how its different services (`forEach`, `filter`, `map` , and `reduce` ) are implemented with lambda expressions. 

The lambda expressions enable us to define dynamic computation, and with the help Java compiler, those expressions are translated internally as generic interfaces, concretized as classes, and instantiated as objects. Sometimes, our lambda expressions follow a typical pattern that can be reduced; the reference methods help us write more concise code. This story will show how to improve our code using reference methods instead of lambda expressions.

In imperative programming (object-oriented programming is one branch of this paradigm), a typical pattern is applied when we use loops with a set of data inside our programs; we apply the same transformations, `forEach`, `filter`, `map` , and `reduce`, to our collection of data. But each of those transformations requires the setting and controlling of the loop, and that setting and maintaining have already been described in functional programming with some patterns. Java has brought and implemented those patterns and named them . This story is going to show how we added to our application.     

---
focus: src/main/java/com/epam/rd/cls/SalesSummaryRow.java
---

### Database description

Our database design is based on a set of sales summarized on a table, where each row ([`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java)) contains the sales of one article ([`Articles`](src/main/java/com/epam/rd/cls/Article.java)) on a region ([`Region`](src/main/java/com/epam/rd/cls/Region.java)) during a period. The database is generated through a helper class [`DBHelper`](src/main/java/com/epam/rd/cls/DBHelper.java) which returns a list of type values [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java), formally: `List<SalesSummaryRow>` is the type of our database data.

---
focus:
---

### A Lambda Expressions Way

A lambda expression is a segment of code that can be stored, passed, and ran it. The following is the syntax to define a lambda expression in Java (also named anonymous function).

```java
(Arguments) -> Function Body
```

If we compare it with a method within a class:

```java
public R functionName(Arguments) {
    // Function Body
}
```

* There are no explicit access modifiers (`public`, `protected`, `private`) of the function.
* There is no explicit return type; this is inferred from the function body.
* `->` is an operator that separates the body arguments from the function.
* There is no need to define a class, and there is no need to instantiate.

> A lambda expression represents a computation.

A lambda expression describes a dynamic computation. Thus you can pass it as arguments or obtain it as a result of a computation.

This story will show how to describe the database's essential operations as a datasheet.

---
focus: src/main/java/com/epam/rd/cls/Main.java:12-18
---

### `forEach` Lambda Service

Let’s apply lambda expressions to the first element of the service in our database: [`foreach`](src/main/java/com/epam/rd/cls/Main.java:12-18). 

The idea behind [`foreach`](src/main/java/com/epam/rd/cls/Main.java:12-18) is to apply a procedure to each database element. A procedure is like a function but doesn't directly produce a value but can produce a side-effect: an IO operation, storing a database element, retrieving an element from a database, modifying the state of a variable, etc. 

A procedure is represented by the `Consumer<T>` interface, a functional interface `@FunctionalInterface` that accepts a value and returns no result.

To concretize this instance in Java, we use a lambda expression, as following snippet of code:

```java
s -> System.out.println(s)
```

* If the parameter list contains a single parameter, you can omit parentheses.
* If the lambda expression's body only contains a single instruction, braces may be omitted.
* The lambda expression has only one parameter.
* The `println` method returns `void`.
* Therefore complies with the `Consumer` interface.

In this case, our [`foreach`](src/main/java/com/epam/rd/cls/Main.java:12-18) receives two parameters: the database (`list`) represented as `List<T>`, and a procedure (`action`) defined as `Consumer<? super T>`, it receives any superclass of `T` and `T` itself. For each element of the database `item` applies the `action` procedure, calling the method to `accept`, which performs the procedure `action` with the `item`. 

**Note:** We know you've already caught the unnecessary line in our implementation, but you will see why we've left it.

---
focus: src/main/java/com/epam/rd/cls/Main.java:20-31
---

### `filter` Lambda Database

Now, we're going to implement the [`filter`](src/main/java/com/epam/rd/cls/Main.java:20-31) service. This service selects a subset of the database base on a predicate. With lambda expression, there is an interface called `Predicate<T>` that takes a value of type `T`, evaluates it, and determines whether or not it satisfies the predicate, returning a `Boolean` value.

To implement this [`filter`](src/main/java/com/epam/rd/cls/Main.java:20-31) service, we need to concretize an instance of `Predicate<T>`. We do not need to create a class that implements that interface, or creates an anonymous object, because it can be implemented using a lambda expression that takes a value and produces a result of type [`Boolean`](src/main/java/com/epam/rd/cls/Main.java:60). 

```java
s -> s.getRegion() == Region.NORTH
```

The argument `s`, with no type set, is inferred to be [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java). The body of the lambda expressions compares if the current `s`([`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java)) is on the region north (`Region.NORTH`). If true, that s is added to the new database (`filteredList`).

---
focus: src/main/java/com/epam/rd/cls/Main.java:32-40
---

### `map` Lambda Database

On this page, we're going to implement the [`map`](src/main/java/com/epam/rd/cls/Main.java:32-40) service. This service applies a function to each database element that transforms that element into another. All database elements are changed. The function is represented as an instance of the interface `Function<T, R>`, which accepts an argument of type `T` and returns a value of type `R`. As we already said, we don't need to define a class that implements that interface or defines an anonymous object, we can determine a lambda expression as follows:

```java
s -> s.getInitialUnits()
```
Where the variable `s` is of type `SalesSummaryRow` and transforms into a value integer type.

The lambda expression's body produces a new value; this value is added to a new database (`mappedList`).

---
focus: src/main/java/com/epam/rd/cls/Main.java:42-50
---

### `reduce` Lambda Database

[`reduce`](src/main/java/com/epam/rd/cls/Main.java:42-50) service takes a database and produces (*reduces*) a value. The service's implementation takes three arguments: the database (`list`); a function (`func`), which represents an operator applied between all database elements; and an initial value (`initial`), which can be returned if the database is empty or it can be computed with first database element with the function. The function is an instance of a function `BiFunction<T, S, R>`, but in this case, is concretized as an instance of `BiFunction<SalesSummaryRow, Double, Double>`; it takes an element from `SalesSummaryRow` and applies a previous result and produces a new development, the function becomes an operator. The lambda expression that is used as an instance of the `BiFunction<SalesSummaryRow, Double, Double>` is:

```java
(s,d) -> s.profit() + d
```

The parameter `s` is an element of the database, `d` is the previous result (or initial value), and the body of the lambda function behaves as an operator; first, obtained from the database element is a projection value that can be applied to the operator (`+`) and the previous result, thus gets a new result.

---
focus:
---

### Summary of Lambda Way

As we did in our previous version, lambda expressions bring a series of interfaces that support the different models of functions; these are defined in the package: `java.util.function,` among which we have used.

The following is the group of interfaces used.

* `Consumer<T>` represents a computation that receives a value and does not produce a result.
* `Predicate<T>`represents a computation that receives a value and verifies a property (predicate).
* `Function<T, R>` represents a computation that receives a value and returns a value.
* `BiFunction<T, S, R>` represents a computation that receives two parameters and returns a value.

Other interfaces in the package can be used in other cases.

---
tag: method_reference-v2.0
focus:
---

### Lambda Way with Reference Methods

Using lambda expression, a feature from functional programming, enables the code's legibility and eliminates repeated code, and functions behave as values. But we can go further to make our code more concise and use new language constructors as method references.

Method references allow replacing some patterns of lambda expressions with syntactic sugar that will enable us to make our code more concise and, therefore, more readable, as we can see on the following pages.

---
focus: src/main/java/com/epam/rd/cls/Main.java:80-81:old
---

### Method reference: object reference

A method reference is not a pointer to a method; it is a form short of writing certain lambda expressions (syntactic sugar). The following lambda expression:

```java
s -> System.out.println(s);
```

Note that the intention is to pass the variable `s` as an argument to the invocation of the `println` method of the `PrintStream` instance
`System.out`. So this can be expressed more compactly:

```java
System.out::println
```

The argument to the `println` method is left implicit and can be converted inversely to the previous lambda expression.

---
focus: src/main/java/com/epam/rd/cls/Main.java:88-91:old
---

### Method reference: class method references

Reference methods are very flexible, allowing you to use object reference as done on the previous page, and we can use specific class methods. It is widespread to find lambda expressions that have the following format:

```java
s -> s.getInitialUnits()
```

In this case, it differs from the version on the previous page because you are referencing a method of argument; in this case, we can use a different version.

```java
SalesSummaryRow::getInitialUnits
```

This version uses the `getInitialUnits()` method of the class `SalesSummaryRow`. We can also see this in [Main](focus:
src/main/java/com/epam/rd/cls/Main.java:88-91:old) with the invocation of the `finalUnits()` method, and we also see it in [Main](focus:src/main/java/com/epam/rd/cls/Main.java:100-103:old) for the request of the `profit()` method.


---
focus: src/main/java/com/epam/rd/cls/Main.java:105-108:old
---

### Method reference: static method references

Method references do not work in all necessary cases, which is why sometimes it is essential to build functions auxiliaries that allow us to manipulate the operations more simply. The following code shows a lambda expression with two parameters (`s`, `r`); in the first, we could use a method reference for the class method, but we can use the second parameter since that method reference is only set for the `SalesSummaryRow` class.

```java
(s,r) -> s.profit() + r
```

You need to define a static method inside the class [`Main`](src/main/java/com/epam/rd/cls/Main.java:67-71). In the method in question, `sumProfit` receives both required parameters and performs the same behavior as the lambda expression above. By so, we already have a static way that can be referenced within the [final computation of the program](src/main/java/com/epam/rd/cls/Main.java:105-107). The parameter passed with this form: `Main::sumProfit` is another reference method, but in this case, with a static method.

---
focus: src/main/java/com/epam/rd/cls/Main.java:57-65
---

### Lambda expressions as return values

Sometimes, it is necessary to have greater flexibility, for that in many situations, you must have the same method with different parameters; in this case, we can create dynamic lambda expressions created on demand. In this case, we have two static methods the critical thing is the value of the return that in both cases will be instances of `Predicate`: [`createPredicateRegion`](src/main/java/com/epam/rd/cls/Main.java:57-60) to create a predicate that checks the predicate and [`createPredicateSalesCost`](src/main/java/com/epam/rd/cls/Main.java:62-65),  which allows us to create a predicate that checks the percentage value between sale and production.

Look at the implementation of the method [`createPredicateRegion`](src/main/java/com/epam/rd/cls/Main.java:57-60) this method returns a concretization of `Predicate`, which compares the region contained in the variable `s` (`SalesSummaryRow`) with the value passed to it by the `region` parameter. This allows you to create dynamic functions later used as predicates inside some [filters](src/main/java/com/epam/rd/cls/Main.java:88-91). This is a way of creating custom functions from another function.

---
focus:
---

###  Summary of A Lambda Way with Reference Methods

Reference methods are excellent for producing more compact code with lambda expressions. Reference methods are neither references nor pointers. The reference methods are plugins and syntactic sugar to make the code more straightforward. Still, in the end, they use lambda expressions since the compiler will translate those references from methods to lambda functions.

There are other method references that we will not show in this case, like

* References to instance methods of higher classes (`super`).
* Class instance constructor references.
* Array constructor references.

---
focus:
---

### Let Lambdas Streams

Until now, each of the implementations of the base service data can be viewed as a total computation over the database. Still, if we look in more detail inside the code of the same, for example, of the `forEach` service, we find that each element applies a specific computation to transform its value. That computation, which is currently represented by a lambda expression, performs the transformation. This means that our computation behaves as an aggregation operation; we have a set of aggregation operations to generate our specific service.

This is the exact definition of a *stream* in Java: “A *stream* is a sequence of data elements supporting operations of *sequential* and *parallel* aggregation.

This means that our services can be implemented directly as a stream.

---
focus:
---

### Database service in Streams

The services we implement: `foreach`, `filter`, `map`, `reduce` and others are already implemented within the *stream*. In this case, we are going to start from our database and generate said *stream*:

```java
List<SalesSumaryRow> dbSales = ...;
dbSales.stream(). ...
```

The above implementation produced on each iteration a new database [`Main`](src/main/java/com/epam/rd/cls/Main.java:88-91:old) in the latest version, a sequence of operations is generated in the reverse order that we had done previously [`Main`](src/main/java/com/epam/rd/cls/Main.java:45-42:old). Note that each operation directly receives the lambda expression, represented by the reference method.

---
focus:
---

### Conclusions

* We have shown a way of, like, a fully oriented world to objects. We can use the characteristics in functional uages such as lambdas ex,pressions.
* Generality not only works in the early stages of object-oriented programming but is also the fundamental basis of programming with lambdas expressions.
