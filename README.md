# Improving Lambda Expressions Story

Improving Lambda Expression Story is the source code to generate the Coding Story named: 
"Improving Lambda Expression Story Java"

This story tells about improving a lambda application with method references and shows all alternatives to this new feature.

This will be translated by program [md2cs](https://github.com/jfcmacro/md2cs).

We assumed that you had installed the program [md2cs](https://github.com/jfcmacro/md2cs)

```shell
$ cd <dir where this repo is installed>
$ md2cs story.md
$ mv target/command-generic-story-java
$ git push
```

